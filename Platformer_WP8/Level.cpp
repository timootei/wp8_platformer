#include "pch.h"
#include "Level.h"

#include <sstream>

#include "Gem.h"
#include "Global.h"
#include "DDSTextureLoader.h"
#include "Helpers\SoundFileReader.h"

using namespace Windows::Devices::Sensors;
using namespace DirectX;

XMFLOAT2 Level::InvalidPosition = XMFLOAT2(-1, -1);

static const int RandomSeed = 354668;

Level::Level(ID3D11Device1 *device, const std::string &levelData, int levelIndex)
	: exit(InvalidPosition)
	, d3dDevice(device)
	, timeRemaining(2 * 60)
	, score(0)
	, reachedExit(false)
{
	LoadTiles(levelData);
	LoadContent(levelIndex);
	srand(RandomSeed);
}

Level::~Level()
{
	for(int y = 0; y < height; ++y)
	{
		for(int x = 0; x < width; ++x)
		{
			delete tiles[y][x];
		}
		delete[] tiles[y];
	}

	delete[] tiles;
}

void Level::LoadContent(int levelIndex)
{
	// Load background layer textures. For now, all levels must
	// use the same backgrounds and only use the left-most part of them.
	for (int i = 0; i < LayersCount; ++i)
	{
		int segmentIndex = levelIndex;
		std::wostringstream ss;
		ss << L"Content\\Backgrounds\\Layer" << i << L"_" << segmentIndex << ".dds";
		DX::ThrowIfFailed(CreateDDSTextureFromFile(d3dDevice, ss.str().c_str(), &layers[i].Resource, &layers[i].ResourceView));
	}

	SoundFileReader exitReachedSoundReader(L"Content\\Sounds\\ExitReached.wav");
	exitReachedSound = Platformer::SharedSoundPlayer()->AddSound(exitReachedSoundReader.GetSoundFormat(), exitReachedSoundReader.GetSoundData());
}

void Level::LoadTiles(const std::string &levelData)
{
	// Load the level and ensure all of the lines are the same length.
	size_t width = 0;
	std::vector<std::string> lines;
	std::string line;
	std::istringstream ss(levelData);

	while(std::getline(ss, line))
	{
		// we subtract 1 from the size to ignore the \r
		size_t lineSize = line.size() - 1;

		if (width == 0) { width = lineSize; }

		if (lineSize != width)
		{
			std::ostringstream os;
			os << "The length of line " << lines.size() << " is different from all preceding lines.";
			throw new std::exception(os.str().c_str());
		}

		lines.push_back(line);
	}

	this->width = width;
	this->height = lines.size();

	// Allocate the tile grid.
	tiles = new Tile**[height];

	// Loop over every tile position,
	for(size_t y = 0; y < height; ++y)
	{
		tiles[y] = new Tile*[width];
		for(size_t x = 0; x < width; ++x)
		{
			// to load each tile.
			char tileType = lines[y][x];
			tiles[y][x] = LoadTile(tileType, x, y);
		}
	}

	if (player == NULL)
		throw new std::exception("A level must have a starting point.");

	if (exit.x == InvalidPosition.x && exit.y == InvalidPosition.y)
		throw new std::exception("A level must have an exit.");
}

Tile* Level::LoadTile(char tileType, int x, int y)
{
	switch (tileType)
	{
	// Blank space
	case '.':
		return new Tile(Texture2D(), Platformer::Passable);

	// Exit
	case 'X':
		return LoadExitTile(x, y);
	
	// Gem
	case 'G':
		return LoadGemTile(x, y);
	
	// Floating platform	
	case '-':
		return LoadTile(L"Platform", Platformer::Platform);

	// Various enemies
	case 'A':
		return LoadEnemyTile(x, y, L"MonsterA");
	case 'B':
		return LoadEnemyTile(x, y, L"MonsterB");
	case 'C':
		return LoadEnemyTile(x, y, L"MonsterC");
	case 'D':
		return LoadEnemyTile(x, y, L"MonsterD");

	// Platform block
	case '~':
		return LoadVarietyTile(L"BlockB", 2, Platformer::Platform);

	// Passable block
	case ':':
		return LoadVarietyTile(L"BlockB", 2, Platformer::Passable);

	// Player 1 start point
	case '1':
		return LoadStartTile(x, y);

	// Impassable block
	case '#':
		return LoadVarietyTile(L"BlockA", 7, Platformer::Impassable);

	default:
		{
			std::ostringstream os;
			os << "Unsupported tile type character '" << tileType << "' at position " << x << ", " << y;
			throw new std::exception(os.str().c_str());
		}
	}
}

Tile* Level::LoadTile(std::wstring name, Platformer::TileCollision collision)
{
	Texture2D tex;
	DX::ThrowIfFailed(CreateDDSTextureFromFile(d3dDevice, (L"Content\\Tiles\\" + name + L".dds").c_str(), &tex.Resource, &tex.ResourceView));
	return new Tile(tex, collision);
}

Tile* Level::LoadVarietyTile(std::wstring baseName, int variationCount, Platformer::TileCollision collision)
{
	int index = rand() % variationCount;
	std::wostringstream wos;
	wos << baseName << index;
	return LoadTile(wos.str(), collision);
}

Tile* Level::LoadStartTile(int x, int y)
{
	if (player != NULL)
		throw new std::exception("A level may only have one starting point.");

	start = GetBounds(x, y).GetBottomCenter();
	player = std::shared_ptr<Player>(new Player(this, start));

	return new Tile(Texture2D(), Platformer::Passable);
}

Tile* Level::LoadExitTile(int x, int y)
{
	if (exit.x != InvalidPosition.x || exit.y != InvalidPosition.y)
		throw new std::exception("A level may only have one exit.");

	exit = GetBounds(x, y).Center();

	return LoadTile(L"Exit", Platformer::Passable);
}

Tile* Level::LoadEnemyTile(int x, int y, std::wstring spriteSet)
{
	XMFLOAT2 position = GetBounds(x, y).GetBottomCenter();
	enemies.push_back(std::shared_ptr<Enemy>(new Enemy(this, position, spriteSet)));

	return new Tile(Texture2D(), Platformer::Passable);
}

Tile* Level::LoadGemTile(int x, int y)
{
	XMFLOAT2 position = GetBounds(x, y).Center();
	gems.push_back(std::shared_ptr<Gem>(new Gem(this, position)));

	return new Tile(Texture2D(), Platformer::Passable);
}

Platformer::TileCollision Level::GetCollision(int x, int y)
{
	if (x < 0 || x >= GetWidth())
		return Platformer::Impassable;

	if (y < 0 || y >= GetHeight())
		return Platformer::Passable;

	return tiles[y][x]->Collision;
}

Rectangle Level::GetBounds(int x, int y)
{
	return Rectangle(x * Tile::Width, y * Tile::Height, Tile::Width, Tile::Height);
}

void Level::Update(GameTime gameTime, AccelerometerReading^ accelState, TouchState touchState, Windows::Graphics::Display::DisplayOrientations orientation)
{
	if (!player->IsAlive() || timeRemaining <= 0)
	{
		// Still want to perform physics on the player.
		player->ApplyPhysics(gameTime);
	}
	else if (reachedExit)
	{
		// Animate the time being converted into points.
		int seconds = (int)round(gameTime.DeltaTime * 100.0f);
		seconds = min(seconds, (int)ceil(timeRemaining));
		timeRemaining -= seconds;
		score += seconds * PointsPerSecond;
	}
	else
	{
		timeRemaining -= gameTime.DeltaTime;
		player->Update(gameTime, touchState, accelState, orientation);
		UpdateGems(gameTime);

		// Falling off the bottom of the level kills the player.
		if (player->GetBoundingRectangle().Top() >= height * Tile::Height)
			OnPlayerKilled(NULL);

		UpdateEnemies(gameTime);

		// The player has reached the exit if they are standing on the ground and
		// his bounding rectangle contains the center of the exit tile. They can only
		// exit when they have collected all of the gems.
		if (player->IsAlive() &&
			player->IsOnGround() &&
			player->GetBoundingRectangle().Contains(exit))
		{
			OnExitReached();
		}
	}

	// Clamp the time remaining at zero.
	if (timeRemaining < 0)
		timeRemaining = 0;
}

void Level::UpdateGems(GameTime gameTime)
{
	for (auto itor = gems.begin(); itor != gems.end();)
	{
		std::shared_ptr<Gem> gem = *itor;

		gem->Update(gameTime);

		if (gem->GetBoundingCircle().Intersects(player->GetBoundingRectangle()))
		{
			itor = gems.erase(itor);
			OnGemCollected(gem, player);
		}
		else
		{
			++ itor;
		}
	}
}

void Level::UpdateEnemies(GameTime gameTime)
{
	for (auto &enemy : enemies)
	{
		enemy->Update(gameTime);

		// Touching an enemy instantly kills the player
		if (enemy->GetBoundingRectangle().Intersects(player->GetBoundingRectangle()))
		{
			OnPlayerKilled(enemy);
		}
	}
}

void Level::OnGemCollected(std::shared_ptr<Gem> gem, std::shared_ptr<Player> collectedBy)
{
	score += Gem::PointValue;

	gem->OnCollected(collectedBy);
}

void Level::OnPlayerKilled(std::shared_ptr<Enemy> killedBy)
{
	player->OnKilled(killedBy);
}

void Level::OnExitReached()
{
	player->OnReachedExit();
	Platformer::SharedSoundPlayer()->PlaySound(exitReachedSound);
	reachedExit = true;
}

void Level::StartNewLife()
{
	player->Reset(start);
}

void Level::Draw(GameTime gameTime, std::shared_ptr<SpriteBatch> spriteBatch)
{
	for (int i = 0; i <= EntityLayer; ++i)
	{
		spriteBatch->Draw(layers[i].ResourceView, XMFLOAT2(0, 0), Colors::White);
	}

	DrawTiles(spriteBatch);

	for (auto &gem : gems)
	{
		gem->Draw(gameTime, spriteBatch);
	}

	player->Draw(gameTime, spriteBatch);

	for (auto &enemy : enemies)
	{
		enemy->Draw(gameTime, spriteBatch);
	}

	for (int i = EntityLayer + 1; i < LayersCount; ++i)
	{
		spriteBatch->Draw(layers[i].ResourceView, XMFLOAT2(0, 0), Colors::White);
	}
}

void Level::DrawTiles(std::shared_ptr<SpriteBatch> spriteBatch)
{
	// For each tile position
	for (int y = 0; y < height; ++y)
	{
		for (int x = 0; x < width; ++x)
		{
			// If there is a visible tile in that position
			Texture2D texture = tiles[y][x]->Texture;
			if (texture.ResourceView != NULL)
			{
				// Draw it in screen space.
				XMFLOAT2 position(x * Tile::Size.x, y * Tile::Size.y);
				spriteBatch->Draw(texture.ResourceView, position, Colors::White);
			}
		}
	}
}