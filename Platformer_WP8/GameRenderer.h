#pragma once
#include "Direct3DBase.h"
#include "DirectXHelper.h"
#include "SpriteBatch.h"
#include "SpriteFont.h"

#include "Helpers\XAudio2SoundPlayer.h"
#include "Helpers\MediaEnginePlayer.h"

#include "TouchState.h"
#include "Level.h"

ref class GameRenderer sealed : public Direct3DBase
{
public:
	GameRenderer(void);

	// Direct3DBase methods.
	virtual void CreateDeviceResources() override;
	virtual void CreateWindowSizeDependentResources() override;
	virtual void Render(float timeTotal, float timeDelta) override;
	
	// Method for updating time-dependent objects.
	void Update(float timeTotal, float timeDelta, Windows::Devices::Sensors::AccelerometerReading^ accelState, TouchState touchState, Windows::Graphics::Display::DisplayOrientations orientation);

private:
	bool m_loadingComplete;
	std::shared_ptr<DirectX::SpriteBatch> spriteBatch;

	// Global content.
	Texture2D winOverlay;
	Texture2D loseOverlay;
	Texture2D diedOverlay;

	std::shared_ptr<DirectX::SpriteFont> hudFont;
	std::unique_ptr<MediaEnginePlayer> mediaPlayer;
	
	// Meta-level game state.
	int levelIndex;
	std::shared_ptr<Level> level;
	bool wasContinuePressed;

	// When the time remaining is less than the warning time, it blinks on the hud
	static const float WarningTime;

	// The number of levels in the Levels directory of our content. We assume that
	// levels in our content are 0-based and that all numbers under this constant
	// have a level file present. This allows us to not need to check for the file
	// or handle exceptions, both of which can add unnecessary time to level loading.
	static const int numberOfLevels = 3;

	void LoadNextLevel();
	void ReloadCurrentLevel();

	void HandleInput(Windows::Devices::Sensors::AccelerometerReading^ accelState, TouchState touchState);

	void DrawHud();
	void DrawShadowedString(std::shared_ptr<DirectX::SpriteFont> font, std::wstring value, DirectX::XMFLOAT2 position, DirectX::FXMVECTOR color);
};

