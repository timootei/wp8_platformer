#pragma once
#include "pch.h"
#include "Direct3DBase.h"
#include <directxmath.h>
#include "SpriteBatch.h"

#include "Circle.h"
#include "Level.h"
#include "Tile.h"
#include "Player.h"
#include "GameTime.h"

#include "Helpers/XAudio2SoundPlayer.h"
#include "Helpers/SoundFileReader.h"

class Gem
{
public:
	/// <summary>
	/// Constructs a new gem.
	/// </summary>
	Gem(Level *level, DirectX::XMFLOAT2 position);

	Level* GetLevel() { return level; }
	
	/// <summary>
	/// Gets the current position of this gem in world space.
	/// </summary>
	DirectX::XMFLOAT2 GetPosition() { return DirectX::XMFLOAT2(basePosition.x, basePosition.y + bounce); }

	/// <summary>
	/// Gets a circle which bounds this gem in world space.
	/// </summary>
	Circle GetBoundingCircle() { return Circle(GetPosition(), Tile::Width / 3.0f); }

	/// <summary>
	/// Bounces up and down in the air to entice players to collect them.
	/// </summary>
	void Update(GameTime gameTime);

	/// <summary>
	/// Draws a gem in the appropriate color.
	/// </summary>
	void Draw(GameTime gameTime, std::shared_ptr<DirectX::SpriteBatch> spriteBatch);

	/// <summary>
    /// Called when this gem has been collected by a player and removed from the level.
    /// </summary>
    /// <param name="collectedBy">
    /// The player who collected this gem. Although currently not used, this parameter would be
    /// useful for creating special powerup gems. For example, a gem could make the player invincible.
    /// </param>
	void OnCollected(std::shared_ptr<Player> collectedBy);

	static const int PointValue = 30;
private:
	/// <summary>
	/// Loads the gem texture and collected sound.
	/// </summary>
	void LoadContent();

	Texture2D texture;
	DirectX::XMFLOAT2 origin;

	// The gem is animated from a base position along the Y axis.
	DirectX::XMFLOAT2 basePosition;
	float bounce;

	Level *level;
	DirectX::FXMVECTOR Color;

	int collectedSound;
};