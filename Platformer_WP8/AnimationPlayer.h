#pragma once
#include "pch.h"

#include <directxmath.h>
#include "SpriteBatch.h"

#include "Animation.h"
#include "GameTime.h"

/// <summary>
/// Controls playback of an Animation.
/// </summary>
class AnimationPlayer
{
public:
	/// <summary>
	/// Gets the animation which is currently playing.
	/// </summary>
	std::shared_ptr<Animation> GetAnimation() { return animation; }

	/// <summary>
	/// Gets the index of the current frame in the animation.
	/// </summary>
	int GetFrameIndex() { return frameIndex; }

	/// <summary>
	/// Gets a texture origin at the bottom center of each frame.
	/// </summary>
	DirectX::XMFLOAT2 GetOrigin() { return DirectX::XMFLOAT2(GetAnimation()->GetFrameWidth() / 2.0f, (float)GetAnimation()->GetFrameHeight()); }

	/// <summary>
	/// Begins or continues playback of an animation.
	/// </summary>
	void PlayAnimation(std::shared_ptr<Animation> animation);

	/// <summary>
	/// Advances the time position and draws the current frame of the animation.
	/// </summary>
	void Draw(GameTime gameTime, std::shared_ptr<DirectX::SpriteBatch> spriteBatch, DirectX::XMFLOAT2 position, DirectX::SpriteEffects effects);

private:
	std::shared_ptr<Animation> animation;
	int frameIndex;

	/// <summary>
	/// The amount of time in seconds that the current frame has been shown for.
	/// </summary>
	float time;
};