#pragma once
#include "pch.h"
#include "Direct3DBase.h"
#include <DirectXMath.h>
#include <vector>
#include <list>

#include "SpriteBatch.h"

#include "Tile.h"
#include "Rectangle.h"
#include "GameTime.h"
#include "TouchState.h"
#include "DirectXHelper.h"

class Gem;
class Enemy;
class Player;

/// <summary>
/// A uniform grid of tiles with collections of gems and enemies.
/// The level owns the player and controls the game's win and lose
/// conditions as well as scoring.
/// </summary>
class Level
{
public:
	std::shared_ptr<Player> GetPlayer() { return player; }
	int GetScore() { return score; }
	bool IsReachedExit() { return reachedExit; }
	float GetTimeRemaining() { return timeRemaining; }
	ID3D11Device1 *GetD3DDevice() { return d3dDevice; }

	/// <summary>
	/// Height of the level measured in tiles.
	/// </summary>
	int GetHeight() { return height; }
	/// <summary>
	/// Width of level measured in tiles.
	/// </summary>
	int GetWidth() { return width; }

	/// <summary>
	/// Constructs a new level.
	/// </summary>
	Level(ID3D11Device1 *device, const std::string &levelData, int levelIndex);
	~Level();

	/// <summary>
	/// Updates all objects in the world, performs collision between them,
	/// and handles the time limit with scoring.
	/// </summary>
	void Update(GameTime gameTime, Windows::Devices::Sensors::AccelerometerReading^ accelState, TouchState touchState, Windows::Graphics::Display::DisplayOrientations orientation);
	
	/// <summary>
	/// Draw everything in the level from background to foreground.
	/// </summary>
	void Draw(GameTime gameTime, std::shared_ptr<DirectX::SpriteBatch> spriteBatch);

	/// <summary>
	/// Gets the collision mode of the tile at a particular location.
	/// This method handles tiles outside of the levels boundaries by making it
	/// impossible to escape past the left or right edges, but allowing things
	/// to jump beyond the top of the level and fall off the bottom.
	/// </summary>
	Platformer::TileCollision GetCollision(int x, int y);

	/// <summary>
	/// Gets the bounding rectangle of a tile in world space.
	/// </summary>
	Rectangle GetBounds(int x, int y);
	
	/// <summary>
	/// Restores the player to the starting point to try the level again.
	/// </summary>
	void StartNewLife();

private:
	void LoadContent(int levelIndex);

	/// <summary>
	/// Iterates over every tile in the structure file and loads its
	/// appearance and behavior. This method also validates that the
	/// file is well-formed with a player start point, exit, etc.
	/// </summary>
	/// <param name="levelData">
	/// A string containing the tile data.
	/// </param>
	void LoadTiles(const std::string &levelData);

	/// <summary>
	/// Loads an individual tile's appearance and behavior.
	/// </summary>
	/// <param name="tileType">
	/// The character loaded from the structure file which
	/// indicates what should be loaded.
	/// </param>
	/// <param name="x">
	/// The X location of this tile in tile space.
	/// </param>
	/// <param name="y">
	/// The Y location of this tile in tile space.
	/// </param>
	/// <returns>The loaded tile.</returns>
	Tile* LoadTile(char tileType, int x, int y);

	/// <summary>
	/// Creates a new tile. The other tile loading methods typically chain to this
	/// method after performing their special logic.
	/// </summary>
	/// <param name="name">
	/// Path to a tile texture relative to the Content/Tiles directory.
	/// </param>
	/// <param name="collision">
	/// The tile collision type for the new tile.
	/// </param>
	/// <returns>The new tile.</returns>
	Tile* LoadTile(std::wstring name, Platformer::TileCollision collision);

	/// <summary>
	/// Loads a tile with a random appearance.
	/// </summary>
	/// <param name="baseName">
	/// The content name prefix for this group of tile variations. Tile groups are
	/// name LikeThis0.png and LikeThis1.png and LikeThis2.png.
	/// </param>
	/// <param name="variationCount">
	/// The number of variations in this group.
	/// </param>
	Tile* LoadVarietyTile(std::wstring name, int variationCount, Platformer::TileCollision collision);

	/// <summary>
	/// Instantiates a player, puts him in the level, and remembers where to put him when he is resurrected.
	/// </summary>
	Tile* LoadStartTile(int x, int y);

	/// <summary>
	/// Remembers the location of the level's exit.
	/// </summary>
	Tile* LoadExitTile(int x, int y);

	/// <summary>
	/// Instantiates an enemy and puts him in the level.
	/// </summary>
	Tile* LoadEnemyTile(int x, int y, std::wstring spriteSet);

	/// <summary>
	/// Instantiates a gem and puts it in the level.
	/// </summary>
	Tile* LoadGemTile(int x, int y);

	/// <summary>
	/// Animates each gem and checks to allows the player to collect them.
	/// </summary>
	void UpdateGems(GameTime gameTime);

	/// <summary>
	/// Animates each enemy and allow them to kill the player.
	/// </summary>
	void UpdateEnemies(GameTime gameTime);

	/// <summary>
	/// Called when a gem is collected.
	/// </summary>
	/// <param name="gem">The gem that was collected.</param>
	/// <param name="collectedBy">The player who collected this gem.</param>
	void OnGemCollected(std::shared_ptr<Gem> gem, std::shared_ptr<Player> collectedBy);

	/// <summary>
	/// Called when the player is killed.
	/// </summary>
	/// <param name="killedBy">
	/// The enemy who killed the player. This is null if the player was not killed by an
	/// enemy, such as when a player falls into a hole.
	/// </param>
	void OnPlayerKilled(std::shared_ptr<Enemy> killedBy);

	/// <summary>
	/// Called when the player reaches the level's exit.
	/// </summary>
	void OnExitReached();

	/// <summary>
	/// Draws each tile in the level.
	/// </summary>
	void DrawTiles(std::shared_ptr<DirectX::SpriteBatch> spriteBatch);

	// Physical structure of the level.
	Tile ***tiles;
	static const int LayersCount = 3;
	Texture2D layers[LayersCount];

	// The layer which entities are drawn on top of.
	static const int EntityLayer = 2;

	// Entities in the level.
	std::shared_ptr<Player> player;
	std::list< std::shared_ptr<Gem> > gems;
	std::list< std::shared_ptr<Enemy> > enemies;

	// Key locations in the level.
	DirectX::XMFLOAT2 start;
	DirectX::XMFLOAT2 exit;
	static DirectX::XMFLOAT2 InvalidPosition;

	int score;
	bool reachedExit;

	float timeRemaining;
	static const int PointsPerSecond = 5;
	ID3D11Device1 *d3dDevice;

	int exitReachedSound;

	int height;
	int width;
};