#pragma once
#include "pch.h"
#include "DirectXHelper.h"

/// <summary>
/// Represents an animated texture.
/// </summary>
/// <remarks>
/// Currently, this class assumes that each frame of animation is
/// as wide as each animation is tall. The number of frames in the
/// animation are inferred from this.
/// </remarks>
class Animation
{
public:
	/// <summary>
	/// All frames in the animation arranged horizontally.
	/// </summary>
	Texture2D GetTexture() { return texture; }
	
	/// <summary>
	/// Duration of time to show each frame.
	/// </summary>
	float GetFrameTime() { return frameTime; }
	
	/// <summary>
	/// When the end of the animation is reached, should it
	/// continue playing from the beginning?
	/// </summary>
	bool IsLooping() { return isLooping; }

	/// <summary>
	/// Gets the number of frames in the animation.
	/// </summary>
	int GetFrameCount() { return textureWidth / GetFrameWidth(); }
	
	/// <summary>
	/// Gets the width of a frame in the animation.
	/// </summary>
	int GetFrameHeight() { return textureHeight; }

	/// <summary>
	/// Gets the height of a frame in the animation.
	/// </summary>
	int GetFrameWidth() { return textureHeight; }
	
	/// <summary>
	/// Constructors a new animation.
	/// </summary>
	Animation(Texture2D texture, float frameTime, bool isLooping)
		: texture(texture)
		, frameTime(frameTime)
		, isLooping(isLooping)
	{
		D3D11_TEXTURE2D_DESC desc = texture.Description();
		textureHeight = desc.Height;
		textureWidth = desc.Width;
	}

private:
	Texture2D texture;
	float frameTime;
	bool isLooping;

	int textureWidth;
	int textureHeight;
};