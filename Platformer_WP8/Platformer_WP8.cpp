﻿#include "pch.h"
#include "Platformer_WP8.h"
#include "BasicTimer.h"
#include "Global.h"

using namespace Windows::ApplicationModel;
using namespace Windows::ApplicationModel::Core;
using namespace Windows::ApplicationModel::Activation;
using namespace Windows::UI::Core;
using namespace Windows::System;
using namespace Windows::Foundation;
using namespace Windows::Graphics::Display;
using namespace concurrency;

Platformer_WP8::Platformer_WP8() :
	m_windowClosed(false),
	m_windowVisible(true)
{
}

void Platformer_WP8::Initialize(CoreApplicationView^ applicationView)
{
	applicationView->Activated +=
		ref new TypedEventHandler<CoreApplicationView^, IActivatedEventArgs^>(this, &Platformer_WP8::OnActivated);

	CoreApplication::Suspending +=
		ref new EventHandler<SuspendingEventArgs^>(this, &Platformer_WP8::OnSuspending);

	CoreApplication::Resuming +=
		ref new EventHandler<Platform::Object^>(this, &Platformer_WP8::OnResuming);

	m_renderer = ref new GameRenderer();

}

void Platformer_WP8::SetWindow(CoreWindow^ window)
{
	window->VisibilityChanged +=
		ref new TypedEventHandler<CoreWindow^, VisibilityChangedEventArgs^>(this, &Platformer_WP8::OnVisibilityChanged);

	window->Closed += 
		ref new TypedEventHandler<CoreWindow^, CoreWindowEventArgs^>(this, &Platformer_WP8::OnWindowClosed);

	window->PointerPressed +=
		ref new TypedEventHandler<CoreWindow^, PointerEventArgs^>(this, &Platformer_WP8::OnPointerPressed);

	window->PointerMoved +=
		ref new TypedEventHandler<CoreWindow^, PointerEventArgs^>(this, &Platformer_WP8::OnPointerMoved);

	window->PointerReleased +=
		ref new TypedEventHandler<CoreWindow^, PointerEventArgs^>(this, &Platformer_WP8::OnPointerReleased);

	DisplayProperties::AutoRotationPreferences = DisplayOrientations::Landscape | DisplayOrientations::LandscapeFlipped;

	DisplayProperties::OrientationChanged +=
		ref new DisplayPropertiesEventHandler(this, &Platformer_WP8::OnOrientationChanged);
	
	m_renderer->Initialize(CoreWindow::GetForCurrentThread());
}

void Platformer_WP8::Load(Platform::String^ entryPoint)
{
}

void Platformer_WP8::Run()
{
	BasicTimer^ timer = ref new BasicTimer();

	while (!m_windowClosed)
	{
		if (m_windowVisible)
		{
			timer->Update();
			CoreWindow::GetForCurrentThread()->Dispatcher->ProcessEvents(CoreProcessEventsOption::ProcessAllIfPresent);
			m_renderer->Update(timer->Total, 0.03333f, Windows::Devices::Sensors::Accelerometer::GetDefault()->GetCurrentReading(), touchState, DisplayProperties::CurrentOrientation);
			m_renderer->Render(timer->Total, 0.03333f);
			m_renderer->Present(); // This call is synchronized to the display frame rate.
		}
		else
		{
			CoreWindow::GetForCurrentThread()->Dispatcher->ProcessEvents(CoreProcessEventsOption::ProcessOneAndAllPending);
		}
	}
}

void Platformer_WP8::Uninitialize()
{
}

void Platformer_WP8::OnVisibilityChanged(CoreWindow^ sender, VisibilityChangedEventArgs^ args)
{
	m_windowVisible = args->Visible;
}

void Platformer_WP8::OnWindowClosed(CoreWindow^ sender, CoreWindowEventArgs^ args)
{
	m_windowClosed = true;
}

void Platformer_WP8::OnPointerPressed(CoreWindow^ sender, PointerEventArgs^ args)
{
	// Insert your code here.
	touchState.IsTouchPressed = true;
}

void Platformer_WP8::OnPointerMoved(CoreWindow^ sender, PointerEventArgs^ args)
{
	// Insert your code here.
}

void Platformer_WP8::OnPointerReleased(CoreWindow^ sender, PointerEventArgs^ args)
{
	// Insert your code here.
	touchState.IsTouchPressed = false;
}

void Platformer_WP8::OnActivated(CoreApplicationView^ applicationView, IActivatedEventArgs^ args)
{
	CoreWindow::GetForCurrentThread()->Activate();
}

void Platformer_WP8::OnOrientationChanged(Platform::Object^ sender)
{
	m_renderer->UpdateForWindowSizeChange();
}

void Platformer_WP8::OnSuspending(Platform::Object^ sender, SuspendingEventArgs^ args)
{
	// Save app state asynchronously after requesting a deferral. Holding a deferral
	// indicates that the application is busy performing suspending operations. Be
	// aware that a deferral may not be held indefinitely. After about five seconds,
	// the app will be forced to exit.
	SuspendingDeferral^ deferral = args->SuspendingOperation->GetDeferral();
	m_renderer->ReleaseResourcesForSuspending();

	create_task([this, deferral]()
	{
		Platformer::SharedSoundPlayer()->Suspend();

		deferral->Complete();
	});
}
 
void Platformer_WP8::OnResuming(Platform::Object^ sender, Platform::Object^ args)
{
	// Restore any data or state that was unloaded on suspend. By default, data
	// and state are persisted when resuming from suspend. Note that this event
	// does not occur if the app was previously terminated.
	 m_renderer->CreateWindowSizeDependentResources();
	 Platformer::SharedSoundPlayer()->Resume();
}

IFrameworkView^ Direct3DApplicationSource::CreateView()
{
	return ref new Platformer_WP8();
}

[Platform::MTAThread]
int main(Platform::Array<Platform::String^>^)
{
	auto direct3DApplicationSource = ref new Direct3DApplicationSource();
	CoreApplication::Run(direct3DApplicationSource);
	return 0;
}
