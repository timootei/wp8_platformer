#pragma once
#include "pch.h"

struct GameTime
{
	float TotalTime;
	float DeltaTime;

	GameTime()
		: TotalTime(0.0f)
		, DeltaTime(0.0f)
	{
	}
	GameTime(float totalTime, float deltaTime) 
		: TotalTime(totalTime)
		, DeltaTime(deltaTime)
	{
	}
};