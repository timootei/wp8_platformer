#include "pch.h"
#include "AnimationPlayer.h"

#include "DirectXHelper.h"
#include "Rectangle.h"

using namespace DirectX;

void AnimationPlayer::PlayAnimation(std::shared_ptr<Animation> theAnimation)
{
	// the same animation as the current one
	if (theAnimation == animation)
	{
		return;
	}

	animation = theAnimation;
	frameIndex = 0;
	time = 0.0f;
}

void AnimationPlayer::Draw(GameTime gameTime, std::shared_ptr<SpriteBatch> spriteBatch, XMFLOAT2 position, SpriteEffects effects)
{
	if (animation == NULL)
	{
		throw new std::exception("No animation is currently playing.");
	}

	// Process passing time.
	time += gameTime.DeltaTime;

	while(time > animation->GetFrameTime())
	{
		time -= animation->GetFrameTime();
		
		// Advance the frame index; looping or clamping as appropriate.
		if (animation->IsLooping())
		{
			frameIndex = (frameIndex + 1) % animation->GetFrameCount();
		}
		else
		{
			frameIndex = min(frameIndex + 1, animation->GetFrameCount() - 1);
		}
	}

	int textureHeight = animation->GetTexture().Description().Height;
	Rectangle source(frameIndex * textureHeight, 0, textureHeight, textureHeight);
	XMFLOAT2 origin = GetOrigin();
	RECT rect = source.ToRect();

	spriteBatch->Draw(animation->GetTexture().ResourceView, position, &rect, DirectX::Colors::White, 0.0f, GetOrigin(), 1.0f, effects);
}