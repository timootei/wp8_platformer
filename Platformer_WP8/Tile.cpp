#include "pch.h"
#include "Tile.h"

const float Tile::Width = 40.0f;
const float Tile::Height = 32.0f;
const DirectX::XMFLOAT2 Tile::Size(Tile::Width, Tile::Height);
