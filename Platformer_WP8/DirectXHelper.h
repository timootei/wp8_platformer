﻿#pragma once

#include <wrl/client.h>
#include <ppl.h>
#include <ppltasks.h>

struct Texture2D
{
	ID3D11Resource *Resource;
	ID3D11ShaderResourceView *ResourceView;

	D3D11_TEXTURE2D_DESC Description() 
	{
		D3D11_TEXTURE2D_DESC desc;
		((ID3D11Texture2D *)Resource)->GetDesc(&desc);

		return desc;
	}

	Texture2D()
		: Resource(NULL)
		, ResourceView(NULL)
	{
	}
};

namespace DX
{
	inline void ThrowIfFailed(HRESULT hr)
	{
		if (FAILED(hr))
		{
			// Set a breakpoint on this line to catch Win32 API errors.
			throw Platform::Exception::CreateException(hr);
		}
	}

	// Function that reads from a binary file asynchronously.
	inline Concurrency::task<Platform::Array<byte>^> ReadDataAsync(Platform::String^ filename)
	{
		using namespace Windows::Storage;
		using namespace Concurrency;
		
		auto folder = Windows::ApplicationModel::Package::Current->InstalledLocation;
		
		return create_task(folder->GetFileAsync(filename)).then([] (StorageFile^ file) 
		{
			return file->OpenReadAsync();
		}).then([] (Streams::IRandomAccessStreamWithContentType^ stream)
		{
			unsigned int bufferSize = static_cast<unsigned int>(stream->Size);
			auto fileBuffer = ref new Streams::Buffer(bufferSize);
			return stream->ReadAsync(fileBuffer, bufferSize, Streams::InputStreamOptions::None);
		}).then([] (Streams::IBuffer^ fileBuffer) -> Platform::Array<byte>^ 
		{
			auto fileData = ref new Platform::Array<byte>(fileBuffer->Length);
			Streams::DataReader::FromBuffer(fileBuffer)->ReadBytes(fileData);
			return fileData;
		});
	}
}

static float Clamp(float value, float min, float max)
{
	return value < min ? min : (value > max ? max : value);
}

static float round(float number)
{
	return number < 0.0f ? ceil(number - 0.5f) : floor(number + 0.5f);
}
