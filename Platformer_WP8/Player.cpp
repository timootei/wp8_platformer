#include "pch.h"
#include "Player.h"

#include <math.h>
#include <string>

#include "Global.h"
#include "DDSTextureLoader.h"
#include "Helpers\SoundFileReader.h"

using namespace DirectX;
using namespace Windows::Devices::Sensors;

const float Player::MoveAcceleration = 13000.0f;
const float Player::MaxMoveSpeed = 1750.0f;
const float Player::GroundDragFactor = 0.48f;
const float Player::AirDragFactor = 0.58f;

const float Player::MaxJumpTime = 0.35f;
const float Player::JumpLaunchVelocity = -3500.0f;
const float Player::GravityAcceleration = 3400.0f;
const float Player::MaxFallSpeed = 550.0f;
const float Player::JumpControlPower = 0.14f; 

const float Player::MoveStickScale = 1.0f;
const float Player::AccelerometerScale = 1.5f;

Player::Player(Level *level, XMFLOAT2 position)
	: level(level)
	, flip(DirectX::SpriteEffects::SpriteEffects_None)
	, movement(0.0f)
{
	LoadContent();
	Reset(position);
}

Rectangle Player::GetBoundingRectangle()
{
	int left = (int)round(Position.x - sprite.GetOrigin().x) + localBounds.X;
	int top = (int)round(Position.y - sprite.GetOrigin().y) + localBounds.Y;

	return Rectangle(left, top, localBounds.Width, localBounds.Height);
}

void Player::LoadContent()
{
	std::wstring baseTexturesPath = L"Content\\Sprites\\Player\\";

	// Load animated textures.
	Texture2D idleAnimationTexture;
	DX::ThrowIfFailed(CreateDDSTextureFromFile(level->GetD3DDevice(), (baseTexturesPath + L"Idle.dds").c_str(), &idleAnimationTexture.Resource, &idleAnimationTexture.ResourceView));
	idleAnimation = std::shared_ptr<Animation>(new Animation(idleAnimationTexture, 0.1f, true));
	
	Texture2D runAnimationTexture;
	DX::ThrowIfFailed(CreateDDSTextureFromFile(level->GetD3DDevice(), (baseTexturesPath + L"Run.dds").c_str(), &runAnimationTexture.Resource, &runAnimationTexture.ResourceView));
	runAnimation = std::shared_ptr<Animation>(new Animation(runAnimationTexture, 0.1f, true));

	Texture2D jumpAnimationTexture;
	DX::ThrowIfFailed(CreateDDSTextureFromFile(level->GetD3DDevice(), (baseTexturesPath + L"Jump.dds").c_str(), &jumpAnimationTexture.Resource, &jumpAnimationTexture.ResourceView));
	jumpAnimation = std::shared_ptr<Animation>(new Animation(jumpAnimationTexture, 0.1f, false));

	Texture2D celebrateAnimationTexture;
	DX::ThrowIfFailed(CreateDDSTextureFromFile(level->GetD3DDevice(), (baseTexturesPath + L"Celebrate.dds").c_str(), &celebrateAnimationTexture.Resource, &celebrateAnimationTexture.ResourceView));
	celebrateAnimation = std::shared_ptr<Animation>(new Animation(celebrateAnimationTexture, 0.1f, false));

	Texture2D dieAnimationTexture;
	DX::ThrowIfFailed(CreateDDSTextureFromFile(level->GetD3DDevice(), (baseTexturesPath + L"Die.dds").c_str(), &dieAnimationTexture.Resource, &dieAnimationTexture.ResourceView));
	dieAnimation = std::shared_ptr<Animation>(new Animation(dieAnimationTexture, 0.1f, false));

	// Calculate bounds within texture size.            
	int width = (int)(idleAnimation->GetFrameWidth() * 0.4);
	int left = (idleAnimation->GetFrameWidth() - width) / 2;
	int height = (int)(idleAnimation->GetFrameWidth() * 0.8);
	int top = idleAnimation->GetFrameWidth() - height;
	localBounds = Rectangle(left, top, width, height);

	// Load Sounds
	SoundFileReader killedSoundReader(L"Content\\Sounds\\PlayerKilled.wav");
	SoundFileReader jumpSoundReader(L"Content\\Sounds\\PlayerJump.wav");
	SoundFileReader fallSoundReader(L"Content\\Sounds\\PlayerFall.wav");

	killedSound = Platformer::SharedSoundPlayer()->AddSound(killedSoundReader.GetSoundFormat(), killedSoundReader.GetSoundData());
	jumpSound = Platformer::SharedSoundPlayer()->AddSound(jumpSoundReader.GetSoundFormat(), jumpSoundReader.GetSoundData());
	fallSound = Platformer::SharedSoundPlayer()->AddSound(fallSoundReader.GetSoundFormat(), fallSoundReader.GetSoundData());
}

void Player::Reset(XMFLOAT2 position)
{
	Position = position;
	Velocity.x = Velocity.y = 0;
	isAlive = true;

	sprite.PlayAnimation(idleAnimation);
}

void Player::Update(GameTime gameTime, TouchState touchState, AccelerometerReading^ accelState, Windows::Graphics::Display::DisplayOrientations orientation)
{
	GetInput(touchState, accelState, orientation);
	ApplyPhysics(gameTime);

	if (isAlive && isOnGround)
	{
		if (abs(Velocity.x) - 0.02f > 0)
		{
			sprite.PlayAnimation(runAnimation);
		}
		else
		{
			sprite.PlayAnimation(idleAnimation);
		}
	}

	// Clear input.
	movement = 0.0f;
	isJumping = false;
}

void Player::GetInput(TouchState touchState, AccelerometerReading^ accelState, Windows::Graphics::Display::DisplayOrientations orientation)
{
	// Move the player with accelerometer
	if (abs(accelState->AccelerationY) > 0.10f)
	{
		// set our movement speed
		movement = Clamp((float)-accelState->AccelerationY * AccelerometerScale, -1.0f, 1.0f);

		// if we're in the LandscapeLeft orientation, we must reverse our movement
		if (orientation == Windows::Graphics::Display::DisplayOrientations::LandscapeFlipped)
		{
			movement = -movement;
		}
	}

	// Check if the player wants to jump.
	isJumping = touchState.IsTouchPressed;
}

void Player::ApplyPhysics(GameTime gameTime)
{
	float elapsed = gameTime.DeltaTime;

	XMFLOAT2 previousPosition = Position;

	// Base velocity is a combination of horizontal movement control and
	// acceleration downward due to gravity.
	Velocity.x += movement * MoveAcceleration * elapsed;
	Velocity.y = Clamp(Velocity.y + GravityAcceleration * elapsed, -MaxFallSpeed, MaxFallSpeed);

	Velocity.y = DoJump(Velocity.y, gameTime);

	// Apply pseudo-drag horizontally.
	if (isOnGround)
		Velocity.x *= GroundDragFactor;
	else
		Velocity.x *= AirDragFactor;

	// Prevent the player from running faster than his top speed.            
	Velocity.x = Clamp(Velocity.x, -MaxMoveSpeed, MaxMoveSpeed);

	// Apply velocity.
	Position.x += (Velocity.x * elapsed);
	Position.y += (Velocity.y * elapsed);
	Position = XMFLOAT2((float)round(Position.x), (float)round(Position.y));

	// If the player is now colliding with the level, separate them.
	HandleCollisions();

	// If the collision stopped us from moving, reset the velocity to zero.
	if (Position.x == previousPosition.x)
		Velocity.x = 0;

	if (Position.y == previousPosition.y)
		Velocity.y = 0;
}

float Player::DoJump(float velocityY, GameTime gameTime)
{
	// If the player wants to jump
	if (isJumping)
	{
		// Begin or continue a jump
		if ((!wasJumping && isOnGround) || jumpTime > 0.0f)
		{
			if (jumpTime == 0.0f)
			{
				Platformer::SharedSoundPlayer()->PlaySound(jumpSound);
			}

			jumpTime += gameTime.DeltaTime;
			sprite.PlayAnimation(jumpAnimation);
		}

		// If we are in the ascent of the jump
		if (0.0f < jumpTime && jumpTime <= MaxJumpTime)
		{
			// Fully override the vertical velocity with a power curve that gives players more control over the top of the jump
			velocityY = JumpLaunchVelocity * (1.0f - (float)pow(jumpTime / MaxJumpTime, JumpControlPower));
		}
		else
		{
			// Reached the apex of the jump
			jumpTime = 0.0f;
		}
	}
	else
	{
		// Continues not jumping or cancels a jump in progress
		jumpTime = 0.0f;
	}
	wasJumping = isJumping;

	return velocityY;
}

void Player::HandleCollisions()
{
	// Get the player's bounding rectangle and find neighboring tiles.
	Rectangle bounds = GetBoundingRectangle();
	int leftTile = (int)floor((float)bounds.Left() / Tile::Width);
	int rightTile = (int)ceil(((float)bounds.Right() / Tile::Width)) - 1;
	int topTile = (int)floor((float)bounds.Top() / Tile::Height);
	int bottomTile = (int)ceil(((float)bounds.Bottom() / Tile::Height)) - 1;

	// Reset flag to search for ground collision.
	isOnGround = false;

	// For each potentially colliding tile,
	for (int y = topTile; y <= bottomTile; ++y)
	{
		for (int x = leftTile; x <= rightTile; ++x)
		{
			// If this tile is collideable,
			Platformer::TileCollision collision = level->GetCollision(x, y);
			if (collision != Platformer::Passable)
			{
				// Determine collision depth (with direction) and magnitude.
				Rectangle tileBounds = level->GetBounds(x, y);
				XMFLOAT2 depth = bounds.GetIntersectionDepth(tileBounds);
				if (depth.x != 0 && depth.y != 0)
				{
					float absDepthX = abs(depth.x);
					float absDepthY = abs(depth.y);

					// Resolve the collision along the shallow axis.
					if (absDepthY < absDepthX || collision == Platformer::Platform)
					{
						// If we crossed the top of a tile, we are on the ground.
						if (previousBottom <= tileBounds.Top())
							isOnGround = true;

						// Ignore platforms, unless we are on the ground.
						if (collision == Platformer::Impassable || isOnGround)
						{
							// Resolve the collision along the Y axis.
							Position = XMFLOAT2(Position.x, Position.y + depth.y);

							// Perform further collisions with the new bounds.
							bounds = GetBoundingRectangle();
						}
					}
					else if (collision == Platformer::Impassable) // Ignore platforms.
					{
						// Resolve the collision along the X axis.
						Position = XMFLOAT2(Position.x + depth.x, Position.y);

						// Perform further collisions with the new bounds.
						bounds = GetBoundingRectangle();
					}
				}
			}
		}
	}

	// Save the new bounds bottom.
	previousBottom = (float)bounds.Bottom();
}

void Player::OnKilled(std::shared_ptr<Enemy> killedBy)
{
	isAlive = false;

	if (killedBy == NULL)
	{
		Platformer::SharedSoundPlayer()->PlaySound(killedSound);
	}
	else
	{
		Platformer::SharedSoundPlayer()->PlaySound(fallSound);
	}

	sprite.PlayAnimation(dieAnimation);
}

void Player::OnReachedExit()
{
	sprite.PlayAnimation(celebrateAnimation);
}

void Player::Draw(GameTime gameTime, std::shared_ptr<SpriteBatch> spriteBatch)
{
	// Flip the sprite to face the way we are moving.
	if (Velocity.x > 0)
		flip = DirectX::SpriteEffects::SpriteEffects_FlipHorizontally;
	else if (Velocity.x < 0)
		flip = DirectX::SpriteEffects::SpriteEffects_None;

	// Draw that sprite.
	sprite.Draw(gameTime, spriteBatch, Position, flip);
}

