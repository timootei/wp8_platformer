@echo off
set TEXCONV=%CD%\texconv.exe
cd Content
echo Starting building the DDS files...
SETLOCAL ENABLEDELAYEDEXPANSION
for /r %%i in (*.png) do (
	set ddspath=%%~di%%~pi%%~ni.dds
	call :do_conversion !ddspath! %%i
)
goto :end
:do_conversion <dds> <png> (
	if "%~t1" == "%~t2" goto :return
	for /F %%i in ('dir /B /O:D %1 %2') do set newest=%%i
	if "%newest%" == "%~n1%~x1" goto :return
	echo Converting "%2" to dds...
	set ddsoutputpath=%~d1%~p1
	if %ddsoutputpath:~-1%==\ set ddsoutputpath=%ddsoutputpath:~0,-1%
	%TEXCONV% -ft DDS -m 1 -pmalpha -o "%ddsoutputpath%" "%2"
	
	:return
	exit /b
)

:end
cd ..