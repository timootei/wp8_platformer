#include "pch.h"
#include "GameRenderer.h"

#include <sstream>
#include <iomanip>
#include <directxcolors.h>

#include "DDSTextureLoader.h"
#include "Helpers\SoundFileReader.h"
#include "Helpers\RandomAccessReader.h"
#include "Tile.h"
#include "Level.h"
#include "Gem.h"

using namespace DirectX;
using namespace Microsoft::WRL;
using namespace Windows::Foundation;
using namespace Windows::UI::Core;
using namespace Windows::Devices::Sensors;

const float GameRenderer::WarningTime = 30;

GameRenderer::GameRenderer(void)
	: levelIndex(-1)
{
	mediaPlayer = std::unique_ptr<MediaEnginePlayer>(new MediaEnginePlayer);
}

void GameRenderer::CreateDeviceResources()
{
	Direct3DBase::CreateDeviceResources();

	spriteBatch = std::shared_ptr<SpriteBatch>(new SpriteBatch(m_d3dContext.Get()));

	DX::ThrowIfFailed(CreateDDSTextureFromFile(m_d3dDevice.Get(), L"Content\\Overlays\\you_died.dds", &diedOverlay.Resource, &diedOverlay.ResourceView));
	DX::ThrowIfFailed(CreateDDSTextureFromFile(m_d3dDevice.Get(), L"Content\\Overlays\\you_lose.dds", &loseOverlay.Resource, &loseOverlay.ResourceView));
	DX::ThrowIfFailed(CreateDDSTextureFromFile(m_d3dDevice.Get(), L"Content\\Overlays\\you_win.dds", &winOverlay.Resource, &winOverlay.ResourceView));

	hudFont = std::unique_ptr<SpriteFont>(new SpriteFont(m_d3dDevice.Get(), L"Content\\Fonts\\Hud.spritefont"));

	Platform::String^ music = "\\Content\\Sounds\\Music.wma";
	Platform::String^ musicPath = Platform::String::Concat(Windows::ApplicationModel::Package::Current->InstalledLocation->Path, music);
	mediaPlayer->Initialize(m_d3dDevice, DXGI_FORMAT_B8G8R8A8_UNORM);  
	mediaPlayer->SetSource(musicPath);

	LoadNextLevel();

	m_loadingComplete = true;
}

void GameRenderer::CreateWindowSizeDependentResources()
{
	Direct3DBase::CreateWindowSizeDependentResources();

}

void GameRenderer::Update(float timeTotal, float timeDelta, AccelerometerReading^ accelState, TouchState touchState, Windows::Graphics::Display::DisplayOrientations orientation)
{
	HandleInput(accelState, touchState);

	GameTime gameTime(timeTotal, timeDelta);

	level->Update(gameTime, accelState, touchState, orientation);
}

void GameRenderer::Render(float timeTotal, float timeDelta)
{
	const float midnightBlue[] = { 0.098f, 0.098f, 0.439f, 1.000f };
	m_d3dContext->ClearRenderTargetView(
		m_renderTargetView.Get(),
		midnightBlue
		);

	m_d3dContext->ClearDepthStencilView(
		m_depthStencilView.Get(),
		D3D11_CLEAR_DEPTH,
		1.0f,
		0
		);

	// Only draw once the resources are loaded (loading is asynchronous).
	if (!m_loadingComplete)
	{
		return;
	}

	m_d3dContext->OMSetRenderTargets(
		1,
		m_renderTargetView.GetAddressOf(),
		m_depthStencilView.Get()
		);

	spriteBatch->Begin(DirectX::SpriteSortMode_Deferred, nullptr, nullptr, nullptr, nullptr, nullptr, m_orientationTransform);

	GameTime gameTime(timeTotal, timeDelta);

	level->Draw(gameTime, spriteBatch);

	DrawHud();
	
	spriteBatch->End();
}

void GameRenderer::LoadNextLevel()
{
	// move to the next level
	levelIndex = (levelIndex + 1) % numberOfLevels;
	
	// Load the level.
	Platform::String^ levelPath = "Content\\Levels\\" + levelIndex + ".txt";
	RandomAccessReader fileReader(levelPath);
	auto fileData = fileReader.Read(fileReader.GetFileSize());
	std::string stringData(reinterpret_cast<char*>(fileData->Data));
	level = std::shared_ptr<Level>(new Level(m_d3dDevice.Get(), stringData, levelIndex));
}

void GameRenderer::HandleInput(AccelerometerReading^ accelState, TouchState touchState)
{
	bool continuePressed = touchState.IsTouchPressed;
	
	// Perform the appropriate action to advance the game and
	// to get the player back to playing.
	if (!wasContinuePressed && continuePressed)
	{
		if (!level->GetPlayer()->IsAlive())
		{
			level->StartNewLife();
		}
		else if (level->GetTimeRemaining() <= 0)
		{
			if (level->IsReachedExit())
			{
				LoadNextLevel();
			}
			else
			{
				ReloadCurrentLevel();
			}
		}
	}

	wasContinuePressed = continuePressed;
}

void GameRenderer::ReloadCurrentLevel()
{
	-- levelIndex;
	LoadNextLevel();
}

void GameRenderer::DrawHud()
{
	Rectangle titleSafeArea(0, 0,
			(int)m_orientationScreenSize.x,
			(int)m_orientationScreenSize.y
		);

	XMFLOAT2 hudLocation(titleSafeArea.X, titleSafeArea.Y);
	XMFLOAT2 center(titleSafeArea.X + titleSafeArea.Width / 2.0f, titleSafeArea.Y + titleSafeArea.Height / 2.0f);

	// Draw time remaining. Uses modulo division to cause blinking when the
	// player is running out of time.
	std::wostringstream wosstream;
	wosstream << L"TIME: " << setfill(L'0') << setw(2) << (int)(level->GetTimeRemaining() / 60) << L":" <<  setfill(L'0') << setw(2) << (int)level->GetTimeRemaining() % 60;
	std::wstring timeString = wosstream.str();
	XMVECTORF32 color;

	if (level->GetTimeRemaining() > WarningTime || level->IsReachedExit() || (int)level->GetTimeRemaining() % 2 ==0)
	{
		color = DirectX::Colors::Yellow;
	}
	else
	{
		color = Colors::Red;
	}

	DrawShadowedString(hudFont, timeString.c_str(), hudLocation, color);

	// Draw score
	wosstream.str(std::wstring());
	wosstream << L"SCORE: " << level->GetScore();
	std::wstring scoreString = wosstream.str();
	float timeHeight = XMVectorGetY(hudFont->MeasureString(scoreString.c_str()));
	DrawShadowedString(hudFont, scoreString, XMFLOAT2(hudLocation.x, hudLocation.y + timeHeight * 1.2f), Colors::Yellow);

	// Determine the status overlay message to show.
	Texture2D status;
	if (level->GetTimeRemaining() == 0)
	{
		if (level->IsReachedExit())
		{
			status = winOverlay;
		}
		else
		{
			status = loseOverlay;
		}
	}
	else if (!level->GetPlayer()->IsAlive())
	{
		status = diedOverlay;
	}

	if (status.ResourceView != NULL)
	{
		// Draw status message
		spriteBatch->Draw(status.ResourceView, XMFLOAT2(center.x - status.Description().Width / 2, center.y - status.Description().Height /2), Colors::White);
	}

	m_d3dDevice->GetCreationFlags();
}

void GameRenderer::DrawShadowedString(std::shared_ptr<SpriteFont> font, std::wstring value, XMFLOAT2 position, FXMVECTOR color)
{
	font->DrawString(spriteBatch.get(), value.c_str(), XMFLOAT2(position.x + 1.0f, position.y + 1.0f), Colors::Black);
	font->DrawString(spriteBatch.get(), value.c_str(), position, color);
}