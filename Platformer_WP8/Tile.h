#pragma once
#include "pch.h"
#include "Direct3DBase.h"
#include <DirectXMath.h>

namespace Platformer
{
    /// <summary>
    /// Controls the collision detection and response behavior of a tile.
    /// </summary>
	enum TileCollision
	{
		/// <summary>
        /// A passable tile is one which does not hinder player motion at all.
        /// </summary>
		Passable = 0,
		
		/// <summary>
        /// An impassable tile is one which does not allow the player to move through
        /// it at all. It is completely solid.
        /// </summary>
		Impassable = 1,
		
		/// <summary>
        /// A platform tile is one which behaves like a passable tile except when the
        /// player is above it. A player can jump up through a platform as well as move
        /// past it to the left and right, but can not fall down through the top of it.
        /// </summary>
		Platform = 2
	};
}

class Tile
{
public:
	Texture2D Texture;
	Platformer::TileCollision Collision;

	static const float Width;
	static const float Height;

	static const DirectX::XMFLOAT2 Size;

	class Tile(Texture2D texture, Platformer::TileCollision collision)
		: Texture(texture)
		, Collision(collision)
	{
	}
};