#pragma once
#include "pch.h"
#include "Direct3DBase.h"
#include <directxmath.h>

#include "Rectangle.h"

struct Circle
{
public:
	DirectX::XMFLOAT2 Center;
	float Radius;

	Circle(DirectX::XMFLOAT2 position, float radius)
		: Center(position)
		, Radius(radius)
	{
	}

	bool Intersects(Rectangle rectangle)
	{
		DirectX::XMFLOAT2 v(Clamp(Center.x, (float)rectangle.Left(), (float)rectangle.Right()), 
							Clamp(Center.y, (float)rectangle.Top(), (float)rectangle.Bottom()));

		DirectX::XMVECTOR direction = DirectX::XMVectorSet(Center.x - v.x, Center.y - v.y, 0, 0);
		float distanceSquared = DirectX::XMVectorGetX(DirectX::XMVector2LengthSq(direction));

		return ((distanceSquared > 0) && (distanceSquared < Radius * Radius));
	}
};