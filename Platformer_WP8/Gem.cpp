#include "pch.h"
#include "Gem.h"
#include <directxcolors.h>

#include "DDSTextureLoader.h"
#include "Global.h"

using namespace DirectX;

Gem::Gem(Level *level, XMFLOAT2 position)
	: level(level)
	, basePosition(position)
	, Color(Colors::Yellow)
{
	LoadContent();
}

void Gem::LoadContent()
{
	SoundFileReader sound(L"Content\\Sounds\\GemCollected.wav");
	collectedSound = Platformer::SharedSoundPlayer()->AddSound(sound.GetSoundFormat(), sound.GetSoundData());

	DX::ThrowIfFailed(CreateDDSTextureFromFile(level->GetD3DDevice(), L"Content\\Sprites\\Gem.dds", &texture.Resource, &texture.ResourceView));

	origin = XMFLOAT2(texture.Description().Width / 2.0f, texture.Description().Height / 2.0f);
}

void Gem::Update(GameTime gameTime)
{
	// Bounce control constants
	const float BounceHeight = 0.18f;
	const float BounceRate = 3.0f;
	const float BounceSync = -0.75f;

	// Bounce along a sine curve over time.
	// Include the X coordinate so that neighboring gems bounce in a nice wave pattern.            
	double t = gameTime.TotalTime * BounceRate + GetPosition().x * BounceSync;
	
	bounce = (float)sin(t) * BounceHeight * texture.Description().Height;
}

void Gem::Draw(GameTime gameTime, std::shared_ptr<DirectX::SpriteBatch> spriteBatch)
{
	spriteBatch->Draw(texture.ResourceView, GetPosition(), NULL, Color, 0.0f, origin);
}

void Gem::OnCollected(std::shared_ptr<Player> collectedBy)
{
	Platformer::SharedSoundPlayer()->PlaySound(collectedSound);
}