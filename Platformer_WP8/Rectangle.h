#pragma once
#include "pch.h"

struct Rectangle
{
public:
	Rectangle() {}
	Rectangle(int x, int y, int width, int height)
		: X(x), Y(y), Width(width), Height(height)
	{}

	int X;
	int Y;
	int Width;
	int Height;

	int Left() { return X; }
	int Right() { return X + Width; }
	int Top() { return Y; }
	int Bottom() { return Y + Height; }

	RECT ToRect() 
	{
		RECT rect;
		rect.left = Left();
		rect.right = Right();
		rect.bottom = Bottom();
		rect.top = Top();

		return rect;
	}

	bool Contains(Rectangle other)
	{
		return other.Left() >= Left() && other.Right() <= Right() &&
			other.Top() >= Top() && other.Bottom() <= Bottom();
	}

	bool Contains(DirectX::XMFLOAT2 vector)
	{
		return vector.x >= Left() && vector.x <= Right() && vector.y >= Top() && vector.y <= Bottom();
	}

	bool Intersects(Rectangle other)
	{
		return other.Left() < Right() && Left() < other.Right() &&
			other.Top() < Bottom() && Top() < other.Bottom();
	}

	// Rectangle extensions

	/// <summary>
	/// Calculates the signed depth of intersection between two rectangles.
	/// </summary>
	/// <returns>
	/// The amount of overlap between two intersecting rectangles. These
	/// depth values can be negative depending on which wides the rectangles
	/// intersect. This allows callers to determine the correct direction
	/// to push objects in order to resolve collisions.
	/// If the rectangles are not intersecting, Vector2.Zero is returned.
	/// </returns>
	DirectX::XMFLOAT2 GetIntersectionDepth(Rectangle rectB)
	{
		// Calculate half sizes.
		float halfWidthA = Width / 2.0f;
		float halfHeightA = Height / 2.0f;
		float halfWidthB = rectB.Width / 2.0f;
		float halfHeightB = rectB.Height / 2.0f;
		
		// Calculate centers.
		DirectX::XMFLOAT2 centerA = DirectX::XMFLOAT2(Left() + halfWidthA, Top() + halfHeightA);
		DirectX::XMFLOAT2 centerB = DirectX::XMFLOAT2(rectB.Left() + halfWidthB, rectB.Top() + halfHeightB);

		// Calculate current and minimum-non-intersecting distances between centers.
		float distanceX = centerA.x - centerB.x;
		float distanceY = centerA.y - centerB.y;
		float minDistanceX = halfWidthA + halfWidthB;
		float minDistanceY = halfHeightA + halfHeightB;

		// If we are not intersecting at all, return (0, 0).
		if (abs(distanceX) >= minDistanceX || abs(distanceY) >= minDistanceY)
			return DirectX::XMFLOAT2(0.0f, 0.0f);

		// Calculate and return intersection depths.
		float depthX = distanceX > 0 ? minDistanceX - distanceX : -minDistanceX - distanceX;
		float depthY = distanceY > 0 ? minDistanceY - distanceY : -minDistanceY - distanceY;
		return DirectX::XMFLOAT2(depthX, depthY);
	}

	/// <summary>
	/// Gets the position of the center of the bottom edge of the rectangle.
	/// </summary>
	DirectX::XMFLOAT2 GetBottomCenter()
	{
		return DirectX::XMFLOAT2(X + Width / 2.0f, (float)Bottom());
	}

	DirectX::XMFLOAT2 Center() 
	{
		return DirectX::XMFLOAT2(X + Width / 2.0f, Y + Height / 2.0f);
	}
};