@echo off
SET AUDIOCLIP=%CD%\AudioClip.exe
cd Content
echo Converting .wma to .wav...
SETLOCAL ENABLEDELAYEDEXPANSION
for /r %%i in (*.wma) do (
	set wmapath=%%~di%%~pi%%~ni.wma
	call :do_conversion !wmapath! %%i
)
goto :end

:do_conversion <wav> <wma> (
	if "%~t1" == "%~t2" goto :return
	for /F %%i in ('dir /B /O:D %1 %2') do set newest=%%i
	if "%newest%" == "%~n1%~x1" goto :return
	echo Converting "%2" to wav...
	%AUDIOCLIP% "%2" "%1"
	
	:return
	exit /b
)

:end
cd ..