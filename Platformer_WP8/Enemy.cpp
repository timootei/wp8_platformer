#include "pch.h"
#include "Enemy.h"

#include <string>

#include "DDSTextureLoader.h"

#include "Tile.h"
#include "Player.h"

const float Enemy::MaxWaitTime = 0.5f;
const float Enemy::MoveSpeed = 64.0f;

Enemy::Enemy(Level *level, DirectX::XMFLOAT2 position, std::wstring spriteSet)
	: level(level)
	, position(position)
	, direction(Platformer::Left)
	, waitTime(0)
{
	LoadContent(spriteSet);
}

void Enemy::LoadContent(std::wstring spriteSet)
{
	// Load animations.
	spriteSet = L"Content\\Sprites\\" + spriteSet + L"\\";

	Texture2D runAnimationTexture;
	DX::ThrowIfFailed(DirectX::CreateDDSTextureFromFile(level->GetD3DDevice(), (spriteSet + L"Run.dds").c_str(), &runAnimationTexture.Resource, &runAnimationTexture.ResourceView));
	runAnimation = std::shared_ptr<Animation>(new Animation(runAnimationTexture, 0.1f, true));

	Texture2D idleAnimationTexture;
	DX::ThrowIfFailed(DirectX::CreateDDSTextureFromFile(level->GetD3DDevice(), (spriteSet + L"Idle.dds").c_str(), &idleAnimationTexture.Resource, &idleAnimationTexture.ResourceView));
	idleAnimation = std::shared_ptr<Animation>(new Animation(idleAnimationTexture, 0.15f, true));

	sprite.PlayAnimation(idleAnimation);

	// Calculate bounds within texture size.
	int width = (int)(idleAnimation->GetFrameWidth() * 0.35);
	int left = (idleAnimation->GetFrameWidth() - width) / 2;
	int height = (int)(idleAnimation->GetFrameWidth() * 0.7);
	int top = idleAnimation->GetFrameHeight() - height;
	localBounds = Rectangle(left, top, width, height);
}

void Enemy::Update(GameTime gameTime)
{
	float elapsed = gameTime.DeltaTime;

	// Calculate tile position based on the side we are walking towards.
	float posX = position.x + localBounds.Width / 2 * (int)direction;
	int tileX = (int)floor(posX / Tile::Width) - (int)direction;
	int tileY = (int)floor(position.y / Tile::Height);

	if (waitTime > 0)
	{
		// Wait for some amount of time.
		waitTime = max(0.0f, waitTime - gameTime.DeltaTime);
		if (waitTime <= 0.0f)
		{
			// Then turn around.
			direction = (Platformer::FaceDirection)(-(int)direction);
		}
	}
	else
	{
		// If we are about to run into a wall or off a cliff, start waiting.
		if (level->GetCollision(tileX + (int)direction, tileY - 1) == Platformer::Impassable ||
			level->GetCollision(tileX + (int)direction, tileY) == Platformer::Passable)
		{
			waitTime = MaxWaitTime;
		}
		else
		{
			// Move in the current direction.
			DirectX::XMFLOAT2 velocity ((int)direction * MoveSpeed * elapsed, 0.0f);
			position.x = position.x + velocity.x;
			position.y = position.y + velocity.y;
		}
	}
}

void Enemy::Draw(GameTime gameTime, std::shared_ptr<DirectX::SpriteBatch> spriteBatch)
{
	// Stop running when the game is paused or before turning around.
	if (!level->GetPlayer()->IsAlive() ||
		level->IsReachedExit() ||
		level->GetTimeRemaining() <= 0 || 
		waitTime > 0)
	{
		sprite.PlayAnimation(idleAnimation);
	}
	else
	{
		sprite.PlayAnimation(runAnimation);
	}

	// Draw facing the way the enemy is moving.
	DirectX::SpriteEffects flip = direction > 0 ? DirectX::SpriteEffects_FlipHorizontally : DirectX::SpriteEffects_None;
	sprite.Draw(gameTime, spriteBatch, position, flip);
}

Rectangle Enemy::GetBoundingRectangle()
{
	int left = (int)round(position.x - sprite.GetOrigin().x) + localBounds.X;
	int top = (int)round(position.y - sprite.GetOrigin().y) + localBounds.Y;

	return Rectangle(left, top, localBounds.Width, localBounds.Height);
}