#pragma once
#include "pch.h"

#include "SpriteBatch.h"

#include "Level.h"
#include "Rectangle.h"
#include "Animation.h"
#include "AnimationPlayer.h"
#include "GameTime.h"

namespace Platformer
{
	/// <summary>
	/// Facing direction along the X axis.
	/// </summary>
	enum FaceDirection
	{
		Left = -1,
		Right = 1
	};
}

/// <summary>
/// A monster who is impeding the progress of our fearless adventurer.
/// </summary>
class Enemy
{
public:
	/// <summary>
	/// Constructs a new Enemy.
	/// </summary>
	Enemy(Level *level, DirectX::XMFLOAT2 position, std::wstring spriteSet);

	Level* GetLevel() { return level; }
	
	/// <summary>
	/// Position in world space of the bottom center of this enemy.
	/// </summary>
	DirectX::XMFLOAT2 GetPosition() { return position; } 
	
	/// <summary>
	/// Gets a rectangle which bounds this enemy in world space.
	/// </summary>
	Rectangle GetBoundingRectangle();

	/// <summary>
	/// Paces back and forth along a platform, waiting at either end.
	/// </summary>
	void Update(GameTime gameTime);

	/// <summary>
	/// Draws the animated enemy.
	/// </summary>
	void Draw(GameTime gameTime, std::shared_ptr<DirectX::SpriteBatch> spriteBatch);

private:
	void LoadContent(std::wstring spriteSet);

	Level *level;
	DirectX::XMFLOAT2 position;
	Rectangle localBounds;
	
	// Animations
	std::shared_ptr<Animation> runAnimation;
	std::shared_ptr<Animation> idleAnimation;
	AnimationPlayer sprite;

	/// <summary>
	/// The direction this enemy is facing and moving along the X axis.
	/// </summary>
	Platformer::FaceDirection direction;

	/// <summary>
	/// How long this enemy has been waiting before turning around.
	/// </summary>
	float waitTime;

	/// <summary>
	/// How long this enemy has been waiting before turning around.
	/// </summary>
	static const float MaxWaitTime;

	/// <summary>
	/// The speed at which this enemy moves along the X axis.
	/// </summary>
	static const float MoveSpeed;
};
