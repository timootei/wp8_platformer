#pragma once
#include "pch.h"

#include "SpriteBatch.h"

#include "Animation.h"
#include "AnimationPlayer.h"
#include "Level.h"
#include "Rectangle.h"
#include "TouchState.h"
#include "GameTime.h"
#include "Enemy.h"

/// <summary>
/// Our fearless adventurer!
/// </summary>
class Player
{
public:
	/// <summary>
	/// Constructors a new player.
	/// </summary>
	Player(Level *level, DirectX::XMFLOAT2 position);

	bool IsAlive() { return isAlive; }
	Level* GetLevel() { return level; }
	
	/// <summary>
	/// Gets whether or not the player's feet are on the ground.
	/// </summary>
	bool IsOnGround() { return isOnGround; }

	/// <summary>
	/// Gets a rectangle which bounds this player in world space.
	/// </summary>
	Rectangle GetBoundingRectangle();

	// Physics state
	DirectX::XMFLOAT2 Position;
	DirectX::XMFLOAT2 Velocity;

	/// <summary>
	/// Handles input, performs physics, and animates the player sprite.
	/// </summary>
	/// <remarks>
	/// We pass in all of the input states so that our game is only polling the hardware
	/// once per frame. We also pass the game's orientation because when using the accelerometer,
	/// we need to reverse our motion when the orientation is in the LandscapeRight orientation.
	/// </remarks>
	void Update(GameTime gameTime, TouchState touchState, Windows::Devices::Sensors::AccelerometerReading^ accelState, Windows::Graphics::Display::DisplayOrientations orientation);

	/// <summary>
	/// Draws the animated player.
	/// </summary>
	void Draw(GameTime gameTime, std::shared_ptr<DirectX::SpriteBatch> spriteBatch);

	/// <summary>
	/// Resets the player to life.
	/// </summary>
	/// <param name="position">The position to come to life at.</param>
	void Reset(DirectX::XMFLOAT2 position);

	/// <summary>
	/// Called when the player has been killed.
	/// </summary>
	/// <param name="killedBy">
	/// The enemy who killed the player. This parameter is null if the player was
	/// not killed by an enemy (fell into a hole).
	/// </param>
	void OnKilled(std::shared_ptr<Enemy> killedBy);

	/// <summary>
	/// Called when this player reaches the level's exit.
	/// </summary>
	void OnReachedExit();

	/// <summary>
	/// Updates the player's velocity and position based on input, gravity, etc.
	/// </summary>
	void ApplyPhysics(GameTime gameTime);

private:
	/// <summary>
	/// Loads the player sprite sheet and sounds.
	/// </summary>
	void LoadContent();
	
	/// <summary>
	/// Gets player horizontal movement and jump commands from input.
	/// </summary>
	void GetInput(TouchState touchState, Windows::Devices::Sensors::AccelerometerReading^ accelState, Windows::Graphics::Display::DisplayOrientations orientation);

	/// <summary>
	/// Calculates the Y velocity accounting for jumping and
	/// animates accordingly.
	/// </summary>
	/// <remarks>
	/// During the accent of a jump, the Y velocity is completely
	/// overridden by a power curve. During the decent, gravity takes
	/// over. The jump velocity is controlled by the jumpTime field
	/// which measures time into the accent of the current jump.
	/// </remarks>
	/// <param name="velocityY">
	/// The player's current velocity along the Y axis.
	/// </param>
	/// <returns>
	/// A new Y velocity if beginning or continuing a jump.
	/// Otherwise, the existing Y velocity.
	/// </returns>
	float DoJump(float velocityY, GameTime gameTime);

	/// <summary>
	/// Detects and resolves all collisions between the player and his neighboring
	/// tiles. When a collision is detected, the player is pushed away along one
	/// axis to prevent overlapping. There is some special logic for the Y axis to
	/// handle platforms which behave differently depending on direction of movement.
	/// </summary>
	void HandleCollisions();

	// Animations
	std::shared_ptr<Animation> idleAnimation;
	std::shared_ptr<Animation> runAnimation;
	std::shared_ptr<Animation> jumpAnimation;
	std::shared_ptr<Animation> celebrateAnimation;
	std::shared_ptr<Animation> dieAnimation;
	DirectX::SpriteEffects flip;
	AnimationPlayer sprite;

	// Sounds
	int killedSound;
	int jumpSound;
	int fallSound;

	Level *level;
	bool isAlive;

	float previousBottom;
	bool isOnGround;

	/// <summary>
	/// Current user movement input.
	/// </summary>
	float movement;

	// Jumping state
	bool isJumping;
	bool wasJumping;
	float jumpTime;

	Rectangle localBounds;

	// Constants for controlling horizontal movement
	static const float MoveAcceleration;
	static const float MaxMoveSpeed;
	static const float GroundDragFactor;
	static const float AirDragFactor;

	// Constants for controlling vertical movement
	static const float MaxJumpTime;
	static const float JumpLaunchVelocity;
	static const float GravityAcceleration;
	static const float MaxFallSpeed;
	static const float JumpControlPower; 

	// Input configuration
	static const float MoveStickScale;
	static const float AccelerometerScale;
};